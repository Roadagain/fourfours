function openedBrackets(fourmula){
    'use strict';

    // count unclosed brackets
    var open = (fourmula.match(/\(/g) || []).length;
    var close = (fourmula.match(/\)/g) || []).length;
    return open - close;
}

function nthBrackets(fourmula, n){
    'use strict';

    // get nth brackets position
    var begin = -1;
    var end = -1;
    var cnt = 0;
    var bcnt = 0;
    var bp = 0;
    for (var i = 0; i < fourmula.length; i++){
        if (fourmula[i] === '('){
            bcnt++;
            cnt++;
            if (bcnt === n){
                begin = i;
                bp = cnt; // log counter to get close bracket position
            }
        }
        else if (fourmula[i] === ')'){
            if (cnt === bp){
                end = i;
                break;
            }
            cnt--;
        }
    }
    if (begin < 0 || end < 0){
        return [];
    }
    else {
        return [begin, end];
    }
}

function minimalizeBrackets(fourmula){
    'use strict';

    // remove needless brackets
    var tmp = fourmula;
    var cnt = 1;
    var brackets = nthBrackets(fourmula, cnt);
    while (brackets.length !== 0){
        // remove nth brackets
        tmp = fourmula.slice(0, brackets[0]) + fourmula.slice(brackets[0] + 1, brackets[1]) + fourmula.slice(brackets[1] + 1);
        try {
            if (eval(fourmula) === eval(tmp)){
                fourmula = tmp;
            }
            else {
                cnt++;
            }
        }
        catch (e){
            // for zero division
            cnt++;
        }
        brackets = nthBrackets(fourmula, cnt);
    }
    return fourmula;
}

function shuffle(array){
    'use strict';

    // return shuffled array
    var i;
    var ary = array.slice();
    i = ary.length;
    if (i === 0){
        return ary;
    }
    while (--i){
        var j = Math.random() * (i + 1) | 0;
        var tmp = ary[j];
        ary[j] = ary[i];
        ary[i] = tmp;
    }
    return ary;
}

function addFourmula(n, fourmula, cnt, four, fours, operands){
    'use strict';

    // add new fourmula
    var op = shuffle(operands);
    for (var i = 0; i < op.length; i++){
        // next fourmulas
        var nfourmula = fourmula + op[i] + four;
        var fourmulas = [
            nfourmula,
            '(' + nfourmula,
            nfourmula + ')',
            fourmula + op[i] + '(' + four, //)
            '(' + nfourmula + ')'
        ];
        for (var j = 0; j < fourmulas.length; j++){
            var tmp = fourFours(n, fourmulas[j], cnt + 1, fours, op);
            if (tmp !== ''){
                return tmp;
            }
        }
    }
    return '';
}

function fourFours(n, fourmula, cnt, fours, operands){
    'use strict';

    if (cnt === 4){
        // close all brackets and minimalize
        var opened = Math.max(0, openedBrackets(fourmula));
        var tmp;
        tmp = minimalizeBrackets(fourmula + new Array(opened).join(')'));

        try {
            if (Math.abs(eval(tmp) - n) < 1e-10){
                // accept too small error
                return tmp;
            }
            else {
                return '';
            }
        }
        catch (e){
            // for zero division
            return '';
        }
    }
    var f;
    for (var i = 0; i < fours.length; i++){
        // get a fourmula includes ith four
        f = addFourmula(n, fourmula, cnt, fours[i], fours, operands);
        if (f.length !== 0){
            return f;
        }
    }
    return '';
}

$(function(){
    'use strict';

    var fours = ['4', '.4', '24', '2']; // select fours from here (24 is 4!, 2 is sqrt(4))
    var operands = [' + ', ' - ', ' * ', ' / ']; // i want power

    $('#fourfours').click(function(){
        $('#fourmula').text('calculating...');
        window.setTimeout(function(){
            var cannot = true;
            var number = Number($('#number').val()) | 0;
            for (var i = 0; i < fours.length; i++){
                var fourmula = fourFours(number, fours[i], 1, fours, operands);
                if (fourmula !== ''){
                    // replace functins to mathmatics operands
                    var str = fourmula.replace(/24/g, '4!').replace(/2/g, '&radic;4');

                    $('#fourmula').html(str + ' = ' + number);
                    cannot = false;
                    break;
                }
            }
            if (cannot === true){
                // sorry
                $('#fourmula').html('Sorry, I cannot make ' + number + ' with four fours.');
            }
        }, 0);
    });
});
